'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
var nodemailer = require('nodemailer');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  updatePrice: async ctx => {
    const { id } = ctx.request.body.id;
    let entity;
    // Find the entry to be update
    if (id) {
      entity = await strapi.services.activities.search(id);
    } else {
      entity = await strapi.services.activities.find(id);
    }
    //update each entery of table
    entity.forEach(element => {
      let activityId = element.id
      let discount = (element.price) * (ctx.request.body.discount / 100)
      let updatedPrice = element.price - discount;
      strapi.query('activities').update({ id: activityId }, { price: updatedPrice });
    });
    return entity.map(entity => sanitizeEntity(entity, { model: strapi.models.activities }));
  },

  async create(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.activities.create(data, { files });
    } else {
      entity = await strapi.services.activities.create(ctx.request.body);
    }

    let entry = sanitizeEntity(entity, { model: strapi.models.activities });
    console.log(entry)
    // send an email by using the email plugin
    await strapi.plugins['email'].services.email.send({
      to: 'gunjankhandal888@gmail.com',
      from: 'gunjankhandal05@gmail.com',
      subject: 'Activity Created  successfully',
      text: `${entry.title} Activity Created Sucessfully`
    });
    return entry;
  },
};
