module.exports = ({ env }) => ({
    // ...
    email: {
      provider: 'sendgrid',
      providerOptions: {
        apiKey:'SEND GRID KEY',
      },
      settings: {
        defaultFrom: 'gunjankhandal05@gmail.com',
        defaultReplyTo: 'gunjankhandal888@gmail.com',
      },
    },
    // ...
  });